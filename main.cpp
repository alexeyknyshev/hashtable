#include <iostream>
#include <fstream>
#include <locale>
#include <chrono>

#include <hashtable_t.h>

class hash
{
public:
    const std::size_t operator()(const std::string &str) const
    {
        unsigned long hash = 5381;
        int c;

        const char *cStr = str.c_str();

        while (c = *cStr++)
            hash = ((hash << 5) + hash) ^ c; // hash(i - 1) * 33 ^ str[i]

        return hash;
    }
};

typedef std::pair<std::string, std::string> Array;

const std::string &arrayFind(Array *arr, int size, const std::string &key)
{
    for (int i = 0; i < size; ++i)
    {
        if (arr[i].first == key)
        {
            return arr[i].second;
        }
    }

    const static std::string stub;
    return stub;
}

int main()
{   
    HashTable<std::string, std::string, hash, 20000> table;

    Array simpleArray[27500];

    const std::string fileName = "table.csv";
    std::ifstream tableFileStream(fileName);
    if (!tableFileStream)
    {
        std::cout << "Could not open \"" << fileName << "\" for reading!" << std::endl;
        return 1;
    }

    int index = 0;
    std::string line;
    while (tableFileStream)
    {
        std::getline(tableFileStream, line);

        size_t sepPos = line.find_first_of(';');
        if (sepPos != std::string::npos)
        {
            const std::string key = line.substr(0, sepPos);
            const std::string val = line.substr(sepPos + 1);

            simpleArray[index].first = key;
            simpleArray[index].second = val;

            ++index;

            table.insert(key, val);
        }
    }

    const std::string key0 = "ТЕХНОСТАР ООО";
    const std::string key1 = "\"ООО \"\"ХЕРАЕУС ЭЛЕКТРО-НАЙТ\"\"\"";
    const std::string key2 = "ИП ГОРАДЗЕ ГАЛИНА АНДРЕЕВНА";
    const std::string key3 = "ФСС Ф-Л N19 МРО (РЕГ.НОМЕР691)";
    const std::string key4 = "НОТАРИУС Г. МОСКВЫ КОЛЕСНИК ИРИНА ЕВГЕНЬЕВНА";
    const std::string key5 = "ООО ТЕХНОТРЕЙД КП МАРЬИНОРОЩИНСКОЕ ОСБ 7981 Г.МОСКВА";
    const std::string key6 = "СПЕЦ ТНП ЮГО-ЗАПАД ООО";
    const std::string key7 = "INSTRUX MANAGEMENT LIMITED";
    const std::string key8 = "ИНДИВИДУАЛЬНЫЙ ПРЕДПРИНИМАТЕЛЬ ГАНЖА ОЛЬГА ОЛЕГОВНА ЛИПЕЦКОЕ ОСБ N 8593 Г ЛИПЕЦК Р/С 40802810435000101096 В ЛИПЕЦКОЕ ОСБ N 8593 Г ЛИПЕЦК";
    const std::string key9 = "ООО РОЗА ВЕТРОВ";

    std::chrono::time_point<std::chrono::system_clock> start, end;
    std::chrono::duration<double> elapsed1, elapsed2;

    start = std::chrono::system_clock::now();/*
    std::cout << "table find: " << table.find(key0) << std::endl;
    std::cout << "table find: " << table.find(key1) << std::endl;
    std::cout << "table find: " << table.find(key2) << std::endl;
    std::cout << "table find: " << table.find(key3) << std::endl;
    std::cout << "table find: " << table.find(key4) << std::endl;
    std::cout << "table find: " << table.find(key5) << std::endl;
    std::cout << "table find: " << table.find(key6) << std::endl;
    std::cout << "table find: " << table.find(key7) << std::endl;
    std::cout << "table find: " << table.find(key8) << std::endl;
    std::cout << "table find: " << table.find(key9) << std::endl;*/
    table.find(key0);
    table.find(key1);
    table.find(key2);
    table.find(key3);
    table.find(key4);
    table.find(key5);
    table.find(key6);
    table.find(key7);
    table.find(key8);
    table.find(key9);
    end   = std::chrono::system_clock::now();

    elapsed1 = end - start;
    std::cout << std::endl;
    std::cout << "table elapsed time: " << elapsed1.count() * 1000 << " ms\n";
    std::cout << std::endl;

    start = std::chrono::system_clock::now();/*
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key0) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key1) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key2) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key3) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key4) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key5) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key6) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key7) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key8) << std::endl;
    std::cout << "array find: " << arrayFind(simpleArray, index + 1, key9) << std::endl;*/
    arrayFind(simpleArray, index + 1, key0);
    arrayFind(simpleArray, index + 1, key1);
    arrayFind(simpleArray, index + 1, key2);
    arrayFind(simpleArray, index + 1, key3);
    arrayFind(simpleArray, index + 1, key4);
    arrayFind(simpleArray, index + 1, key5);
    arrayFind(simpleArray, index + 1, key6);
    arrayFind(simpleArray, index + 1, key7);
    arrayFind(simpleArray, index + 1, key8);
    arrayFind(simpleArray, index + 1, key9);
    end   = std::chrono::system_clock::now();

    elapsed2 = end - start;
    std::cout << std::endl;
    std::cout << "array elapsed time: " << elapsed2.count() * 1000 << " ms\n";
    std::cout << std::endl;

    double diffTimes = elapsed2.count() / elapsed1.count();
    std::cout << "winning " << diffTimes << " times" << std::endl;


    return 0;
}

