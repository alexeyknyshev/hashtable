#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <array>
#include <set>

#include <functional>

template<typename Key, typename Value, typename HashFunc, size_t HashTableSize = 1024>
class HashTable
{
public:
    void insert(Key key, Value val);
    bool erase(Key key);
    bool contatins(Key key);
    const Value &find(Key key) const;

private:
    struct KeyValuePair
    {
        KeyValuePair(Key k, Value v) : key(k), value(v) { }

        Key key;
        Value value;

        inline bool operator ==(const KeyValuePair &other) const
        { return key == other.key; }

        inline bool operator <(const KeyValuePair &other) const
        { return key < other.key; }
    };

    std::array<std::set<KeyValuePair>, HashTableSize> mData;
    static HashFunc mHashFunc;
};

template<typename Key, typename Value, typename HashFunc, size_t HashTableSize>
void HashTable<Key, Value, HashFunc, HashTableSize>::insert(Key key, Value val)
{
    size_t _hash = mHashFunc(key) % HashTableSize;
    std::set<KeyValuePair> &set = mData[_hash];
    set.insert(KeyValuePair(key, val));
}

template<typename Key, typename Value, typename HashFunc, size_t HashTableSize>
bool HashTable<Key, Value, HashFunc, HashTableSize>::erase(Key key)
{
    size_t _hash = mHashFunc(key) % HashTableSize;
    std::set<KeyValuePair> &set = mData[_hash];
    return (set.erase(key) != 0);
}

template<typename Key, typename Value, typename HashFunc, size_t HashTableSize>
bool HashTable<Key, Value, HashFunc, HashTableSize>::contatins(Key key)
{
    size_t _hash = mHashFunc(key) % HashTableSize;
    std::set<KeyValuePair> &set = mData[_hash];
    return (set.count(KeyValuePair(key, Value())) != 0);
}

template<typename Key, typename Value, typename HashFunc, size_t HashTableSize>
const Value &HashTable<Key, Value, HashFunc, HashTableSize>::find(Key key) const
{
    size_t _hash = mHashFunc(key) % HashTableSize;
    const std::set<KeyValuePair> &set = mData[_hash];

    const auto it = set.find(KeyValuePair(key, Value()));
    if (it != set.end())
    {
        return it->value;
    }

    const static Value placeholder;
    return placeholder;
}

#endif // HASHTABLE_H
